function! myspacevim#before() abort
  let g:neomake_python_python_exe = '/usr/bin/python3'
endfunction

function! myspacevim#after() abort
endfunction
